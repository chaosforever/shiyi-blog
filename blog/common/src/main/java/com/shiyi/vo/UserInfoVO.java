package com.shiyi.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author blue
 * @date 2022/1/5
 * @apiNote
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserInfoVO {

    /**
     * 用户账号id
     */
    private String id;

    /**
     * 邮箱号
     */
    private String email;

    /**
     * 登录方式
     */
    private Integer loginType;


    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 用户简介
     */
    private String intro;

    /**
     * 个人网站
     */
    private String webSite;

    /**
     * 文章量
     */
    private int articleCount;

    /**
     * 收藏量
     */
    private int collectCount;

    /**
     * 笔记量
     */
    private int noteCount;

    /**
     * 评论量
     */
    private int commentCount;

    /**
     * 粉丝量
     */
    private int fansCount;


    /**
     * 粉丝量
     */
    private int watchCount;

    /**
     * 七天关注量
     */
    private int qiDayFollowedCount;

    /**
     * token
     */
    private String token;


}
